dnl
dnl $Id: acinclude.m4,v 1.3 2009-02-09 23:10:20 hehi Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
  AC_MSG_CHECKING(whether to make debug objects)
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-g,,'`
  else
    case $CXXFLAGS in
    *-g*) ;;
    *)    CXXFLAGS="$CXXFLAGS -g" ;;
    esac
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])

  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],[Enable optimization of objects])])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" ; then
    case $CXXFLAGS in
    *-O*) ;;
    *)    CXXFLAGS="$CXXFLAGS -O2" ;;
    esac
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo   $CFLAGS   | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
    CXXFLAGS=`echo $CXXFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_PROFILING],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
	
  AC_ARG_ENABLE([profiling],
	        [AC_HELP_STRING([--enable-profiling],
			        [Compile code to enable profiling])],
                [],[enable_profiling=no])
  AC_MSG_CHECKING([whether to enable profiling])
  if test "x$enable_profiling" = "xyes" ; then 
    CFLAGS="$CFLAGS -pg" 
    CXXFLAGS="$CXXFLAGS -pg"
    LDFLAGS="$LDFLAGS -pg"
  fi
  AC_MSG_RESULT([$enable_profiling])
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_STRICT],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
	
  AC_ARG_ENABLE([strict],
	        [AC_HELP_STRING([--enable-strict],
			        [Require strictly correct code])],
                [],[enable_strict=no])
  AC_MSG_CHECKING([whether require strictly correct code])
  if test "x$enable_strict" = "xyes" ; then 
    CFLAGS="$CFLAGS -Wall -Werror -pedantic -ansi" 
    # Cannot use `-pedantic' due to use of `long long'
    CXXFLAGS="$CXXFLAGS -Wall -Werror -ansi"
  fi
  AC_MSG_RESULT([$enable_strict ($CFLAGS)])
])
  
dnl ------------------------------------------------------------------

dnl
dnl $Id: acinclude.m4,v 1.3 2009-02-09 23:10:20 hehi Exp $
dnl $Author: hehi $
dnl $Date: 2009-02-09 23:10:20 $
dnl
dnl Autoconf macro to check for existence or ROOT on the system
dnl Synopsis:
dnl
dnl  ROOT_PATH([MINIMUM-VERSION, [ACTION-IF-FOUND, [ACTION-IF-NOT-FOUND]]])
dnl
dnl Some examples: 
dnl 
dnl    ROOT_PATH(3.03/05, , AC_MSG_ERROR(Your ROOT version is too old))
dnl    ROOT_PATH(, AC_DEFINE([HAVE_ROOT]))
dnl 
dnl The macro defines the following substitution variables
dnl
dnl    ROOTCONF           full path to root-config
dnl    ROOTEXEC           full path to root
dnl    ROOTCINT           full path to rootcint
dnl    ROOTLIBDIR         Where the ROOT libraries are 
dnl    ROOTINCDIR         Where the ROOT headers are 
dnl    ROOTCFLAGS         Extra compiler flags
dnl    ROOTLIBS           ROOT basic libraries 
dnl    ROOTGLIBS          ROOT basic + GUI libraries
dnl    ROOTAUXLIBS        Auxilary libraries and linker flags for ROOT
dnl    ROOTAUXCFLAGS      Auxilary compiler flags 
dnl    ROOTRPATH          Same as ROOTLIBDIR
dnl
dnl The macro will fail if root-config and rootcint isn't found.
dnl
dnl Christian Holm Christensen <cholm@nbi.dk>
dnl
AC_DEFUN([ROOT_PATH],
[
  AC_ARG_WITH(rootsys,
  [  --with-rootsys          top of the ROOT installation directory],
    user_rootsys=$withval,
    user_rootsys="none")
  if test ! x"$user_rootsys" = xnone; then
    rootbin="$user_rootsys/bin"
  elif test ! x"$ROOTSYS" = x ; then 
    rootbin="$ROOTSYS/bin"
  else 
   rootbin=$PATH
  fi
  AC_PATH_PROG(ROOTCONF, root-config , no, $rootbin)
  AC_PATH_PROG(ROOTEXEC, root , no, $rootbin)
  AC_PATH_PROG(ROOTCINT, rootcint , no, $rootbin)
	
  if test ! x"$ROOTCONF" = "xno" && \
     test ! x"$ROOTCINT" = "xno" ; then 

    # define some variables 
    ROOTLIBDIR=`$ROOTCONF --libdir`
    ROOTINCDIR=`$ROOTCONF --incdir`
    ROOTCFLAGS=`$ROOTCONF --noauxcflags --cflags` 
    ROOTLIBS=`$ROOTCONF --noauxlibs --noldflags --libs`
    ROOTGLIBS=`$ROOTCONF --noauxlibs --noldflags --glibs`
    ROOTAUXCFLAGS=`$ROOTCONF --auxcflags`
    ROOTAUXLIBS=`$ROOTCONF --auxlibs`
    ROOTRPATH=$ROOTLIBDIR
	
    if test $1 ; then 
      AC_MSG_CHECKING(wether ROOT version >= [$1])
      vers=`$ROOTCONF --version | tr './' ' ' | awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      requ=`echo $1 | tr './' ' ' | awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      if test $vers -lt $requ ; then 
        AC_MSG_RESULT(no)
	no_root="yes"
      else 
        AC_MSG_RESULT(yes)
      fi
    fi
  else
    # otherwise, we say no_root
    no_root="yes"
  fi

  AC_SUBST(ROOTLIBDIR)
  AC_SUBST(ROOTINCDIR)
  AC_SUBST(ROOTCFLAGS)
  AC_SUBST(ROOTLIBS)
  AC_SUBST(ROOTGLIBS) 
  AC_SUBST(ROOTAUXLIBS)
  AC_SUBST(ROOTAUXCFLAGS)
  AC_SUBST(ROOTRPATH)

  if test "x$no_root" = "x" ; then 
    ifelse([$2], , :, [$2])     
  else 
    ifelse([$3], , :, [$3])     
  fi
])

dnl __________________________________________________________________
dnl
dnl AC_RCUXX([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUXX],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcuxx],
        [AC_HELP_STRING([--with-rcuxx],	[Prefix where Rcu++ is installed])],
	[],[with_rcuxx="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcuxx-url],
        [AC_HELP_STRING([--with-rcuxx-url],
		[Base URL where the Rcu++ dodumentation is installed])],
        rcuxx_url=$withval, rcuxx_url="")
    if test "x${RCUXX_CONFIG+set}" != xset ; then 
        if test "x$with_rcuxx" != "xno" ; then 
	    RCUXX_CONFIG=$with_rcuxx/bin/rcuxx-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_rcuxx" != "xno" ; then 
        AC_PATH_PROG(RCUXX_CONFIG, rcuxx-config, no)
        rcuxx_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for Rcu++ version >= $rcuxx_min_version)

        # Check if we got the script
        with_rcuxx=no    
        if test "x$RCUXX_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           RCUXX_CPPFLAGS=`$RCUXX_CONFIG --cppflags`
           RCUXX_INCLUDEDIR=`$RCUXX_CONFIG --includedir`
           RCUXX_LIBS=`$RCUXX_CONFIG --libs`
           RCUXX_LTLIBS=`$RCUXX_CONFIG --ltlibs`
           RCUXX_LIBDIR=`$RCUXX_CONFIG --libdir`
           RCUXX_LDFLAGS=`$RCUXX_CONFIG --ldflags`
           RCUXX_LTLDFLAGS=`$RCUXX_CONFIG --ltldflags`
           RCUXX_PREFIX=`$RCUXX_CONFIG --prefix`
           
           # Check the version number is OK.
           rcuxx_version=`$RCUXX_CONFIG -V` 
           rcuxx_vers=`echo $rcuxx_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           rcuxx_regu=`echo $rcuxx_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $rcuxx_vers -ge $rcuxx_regu ; then 
                with_rcuxx=yes
           fi
        fi
        AC_MSG_RESULT($with_rcuxx - is $rcuxx_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_RCUXX, [Whether we have rcuxx])
    
    
        if test "x$with_rcuxx" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
            LDFLAGS="$LDFLAGS $RCUXX_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $RCUXX_CPPFLAGS"
     
            # Change the language 
            AC_LANG_PUSH(C++)
    
     	    # Check for a header 
            have_rcuxx_rcu_h=0
            AC_CHECK_HEADER([rcuxx/Rcu.h], [have_rcuxx_rcu_h=1])
    
            # Check the library. 
            have_librcuxx=no
            AC_MSG_CHECKING(for -lrcuxx)
            AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <rcuxx/Rcu.h>],
                                            [Rcuxx::Rcu::Open("foo")])], 
                                            [have_librcuxx=yes])
            AC_MSG_RESULT($have_librcuxx)
    
            if test $have_rcuxx_rcu_h -gt 0    && \
                test "x$have_librcuxx"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_RCUXX)
            else 
                with_rcuxx=no
            fi
            # Change the language 
            AC_LANG_POP(C++)
    	CPPFLAGS=$save_CPPFLAGS
    	LDFLAGS=$save_LDFLAGS
        fi
    
        AC_MSG_CHECKING(where the Rcu++ documentation is installed)
        if test "x$rcuxx_url" = "x" && \
    	test ! "x$RCUXX_PREFIX" = "x" ; then 
           RCUXX_URL=${RCUXX_PREFIX}/share/doc/rcuxx/html
        else 
    	RCUXX_URL=$rcuxx_url
        fi	
        AC_MSG_RESULT($RCUXX_URL)
    fi
   
    if test "x$with_rcuxx" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUXX_URL)
    AC_SUBST(RCUXX_PREFIX)
    AC_SUBST(RCUXX_CPPFLAGS)
    AC_SUBST(RCUXX_INCLUDEDIR)
    AC_SUBST(RCUXX_LDFLAGS)
    AC_SUBST(RCUXX_LIBDIR)
    AC_SUBST(RCUXX_LIBS)
    AC_SUBST(RCUXX_LTLIBS)
    AC_SUBST(RCUXX_LTLDFLAGS)
])
dnl
dnl EOF
dnl 


dnl __________________________________________________________________
dnl
dnl AC_READRAW([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_READRAW],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([readraw-prefix],
        [AC_HELP_STRING([--with-readraw-prefix],
		[Prefix where ReadRaw is installed])],
        readraw_prefix=$withval, readraw_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([readraw-url],
        [AC_HELP_STRING([--with-readraw-url],
		[Base URL where the ReadRaw dodumentation is installed])],
        readraw_url=$withval, readraw_url="")
    if test "x${READRAW_CONFIG+set}" != xset ; then 
        if test "x$readraw_prefix" != "x" ; then 
	    READRAW_CONFIG=$readraw_prefix/bin/readraw-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(READRAW_CONFIG, readraw-config, no)
    readraw_min_version=ifelse([$1], ,0.3,$1)
    
    # Message to user
    AC_MSG_CHECKING(for ReadRaw version >= $readraw_min_version)

    # Check if we got the script
    readraw_found=no    
    if test "x$READRAW_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       READRAW_CPPFLAGS=`$READRAW_CONFIG --cppflags`
       READRAW_INCLUDEDIR=`$READRAW_CONFIG --includedir`
       READRAW_LIBS=`$READRAW_CONFIG --libs`
       READRAW_LTLIBS=`$READRAW_CONFIG --ltlibs`
       READRAW_LIBDIR=`$READRAW_CONFIG --libdir`
       READRAW_LDFLAGS=`$READRAW_CONFIG --ldflags`
       READRAW_LTLDFLAGS=`$READRAW_CONFIG --ltldflags`
       READRAW_PREFIX=`$READRAW_CONFIG --prefix`
       
       # Check the version number is OK.
       readraw_version=`$READRAW_CONFIG -V` 
       readraw_vers=`echo $readraw_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       readraw_regu=`echo $readraw_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $readraw_vers -ge $readraw_regu ; then 
            readraw_found=yes
       fi
    fi
    AC_MSG_RESULT($readraw_found - is $readraw_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_READRAW_READER_H, 
                [Whether we have readraw/Reader.h header])
    AH_TEMPLATE(HAVE_READRAW, [Whether we have readraw])


    if test "x$readraw_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS $READRAW_LDFLAGS"
    	CPPFLAGS="$CPPFLAGS $READRAW_CPPFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_readraw_reader_h=0
        AC_CHECK_HEADER([readraw/Reader.h], [have_readraw_reader_h=1])

        # Check the library. 
        have_libreadraw=no
        AC_MSG_CHECKING(for -lreadraw)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <readraw/Reader.h>],
                        [unsigned long w; ReadRaw::Reader::SwapBytes(w)])], 
                        [have_libreadraw=yes])
        AC_MSG_RESULT($have_libreadraw)

        if test $have_readraw_reader_h -gt 0    && \
            test "x$have_libreadraw"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_READRAW_READER_H)
            AC_DEFINE(HAVE_READRAW)
        else 
            readraw_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
	CPPFLAGS=$save_CPPFLAGS
	LDFLAGS=$save_LDFLAGS
    fi

    AC_MSG_CHECKING(where the ReadRaw documentation is installed)
    if test "x$readraw_url" = "x" && \
	test ! "x$READRAW_PREFIX" = "x" ; then 
       READRAW_URL=${READRAW_PREFIX}/share/doc/readraw/html
    else 
	READRAW_URL=$readraw_url
    fi	
    AC_MSG_RESULT($READRAW_URL)
   
    if test "x$readraw_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(READRAW_URL)
    AC_SUBST(READRAW_PREFIX)
    AC_SUBST(READRAW_CPPFLAGS)
    AC_SUBST(READRAW_INCLUDEDIR)
    AC_SUBST(READRAW_LDFLAGS)
    AC_SUBST(READRAW_LIBDIR)
    AC_SUBST(READRAW_LIBS)
    AC_SUBST(READRAW_LTLIBS)
    AC_SUBST(READRAW_LTLDFLAGS)
])

dnl __________________________________________________________________
dnl
dnl AC_ALTROCC([ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ALTROCC],
[
    AC_ARG_WITH([altrocc], 
    	    [AC_HELP_STRING([--with-altrocc],
    	                    [Prefix of ALTROCC installation])])
    save_LDFLAGS=$LDFLAGS
    save_CPPFLAGS=$CPPFLAGS
    if test ! "x$with_altrocc" = "x" && test ! "x$with_altrocc" = "xno" ; then 
       LDFLAGS="$LDFLAGS -L$with_altrocc/lib/altrocc" 
       CPPFLAGS="$CPPFLAGS -I$with_altrocc/include"
    else
       with_altrocc=
    fi
    have_altrocc_compiler_h=0
    AH_TEMPLATE(HAVE_ALTROCC_COMPILER_H, [Whether we have ALTROCC header])
    AC_CHECK_HEADER([altrocc/compiler.h], [have_altrocc_compiler_h=1])
    have_libaltrocc=0
    AC_CHECK_LIB([altrocc], [RCUC_compile], [have_libaltrocc=1])
    if test $have_libaltrocc -gt 0 && \
	test $have_altrocc_compiler_h -gt 0; then 
       AC_DEFINE(HAVE_ALTROCC_COMPILER_H)
       if test ! "x$with_altrocc" = "x" ; then 
          ALTROCCLDFLAGS="-L$with_altrocc/lib"
          ALTROCCCPPFLAGS="-I$with_altrocc/include"
       fi
       with_altrocc=yes
       ALTROCCLIB=-laltrocc
    else 
       with_altrocc=no
       ALTROCCLDFLAGS=
       ALTROCCPPFLAGS=
    fi				     
    LDFLAGS="$save_LDFLAGS"
    CPPFLAGS="$save_CPPFLAGS"

    if test "x$with_altrocc" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else 
        ifelse([$2], , :, [$2])
    fi
    AC_SUBST([ALTROCCLDFLAGS])
    AC_SUBST([ALTROCCCPPFLAGS])
    AC_SUBST([ALTROCCLIB])
])
dnl
dnl EOF
dnl 

dnl ------------------------------------------------------------------
dnl
dnl Check for Oracle
dnl AC_ORACLE([ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
dnl
AC_DEFUN([AC_ORACLE],
[
  AC_ARG_WITH([oracle-prefix],
	      [AC_HELP_STRING([--with-oracle-prefix=DIR],
		[Specify Oracle client installation prefix])])
  AC_ARG_WITH([oracle-libdir],
	      [AC_HELP_STRING([--with-oracle-libdir=DIR],
	        [Specify Oracle client library installation path])])
  AC_ARG_WITH([oracle-incdir],
	      [AC_HELP_STRING([--with-oracle-incdir=DIR],
		[Specify Oracle client header installation path])])

  AC_LANG_PUSH([C++])
  save_LDFLAGS=$LDFLAGS
  save_CPPFLAGS=$CPPFLAGS
  if test ! "x$with_oracle_prefix" = "x" && \
     test ! "x$with_oracle_prefix" = "xyes" ; then 
    with_oracle_libdir=$with_oracle_prefix
    with_oracle_incdir=$with_oracle_prefix/include
  fi
  if test ! "x$with_oracle_libdir" = "x" && \
     test ! "x$with_oracle_libdir" = "xyes" ; then 
    ORACLE_LDFLAGS="-L$with_oracle_libdir -Wl,-rpath,$with_oracle_libdir"
  fi
  if test ! "x$with_oracle_incdir" = "x" && \
     test ! "x$with_oracle_incdix" = "xyes" ; then 
    ORACLE_CPPFLAGS="-I$with_oracle_incdir"
  fi
  LDFLAGS="$LDFLAGS $ORACLE_LDFLAGS"
  CPPFLAGS="$CPPFLAGS $ORACLE_CPPFLAGS"
  have_oracle=yes
  save_LIBS="$LIBS"
  dnl AC_CHECK_LIB([nnz10],[main],[],[have_oracle=no], [-lssl])
  AC_CHECK_LIB([clntsh],[main],[],[have_oracle=no], [-lnnz10])
  AC_CHECK_LIB([ociei],[main],[],[have_oracle=no], [-lclntsh -lnnz10])
  AC_CHECK_LIB([occi],[main],[],[have_oracle=no], [-lclntsh -lnnz10 -lociei])
  AC_CHECK_HEADER([occi.h],[],[have_oracle=no])
  AC_MSG_CHECKING([whether to compile in Oracle support])
  AC_MSG_RESULT($have_oracle)
  AC_LANG_POP([C++])
  LDFLAGS="$save_LDFLAGS"
  CPPFLAGS="$save_CPPFLAGS"
  if test "x$have_oracle" = "xyes" ;  then 
    ORACLE_LIBS="-locci -lociei -lclntsh -lnnz10"
  else
    ORACLE_LDFLAGS=""
    ORACLE_CPPFLAGS=""
  fi
  LIBS="$save_LIBS"
  if test "x$have_oracle" = "xyes" ; then 
    ifelse([$1], , :, [$1])
  else 
    ifelse([$2], , :, [$2])
  fi
  AC_SUBST(ORACLE_LDFLAGS)
  AC_SUBST(ORACLE_CPPFLAGS)
  AC_SUBST(ORACLE_LIBS)
])

dnl ------------------------------------------------------------------
dnl
dnl Check for Mysql
dnl AC_MYSQL([MIN_VERSION=5.0.0
dnl            [, ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]]])
dnl
AC_DEFUN([AC_MYSQL],
[
  AC_ARG_WITH([mysql-prefix],
	      [AC_HELP_STRING([--with-mysql-prefix=DIR],
		[Specify Mysql client installation prefix])], 
	      [with_mysql_prefix=$withval],[with_mysql_prefix=none])

  if test ! x"$with_mysql_prefix" = xnone; then
    mysql_bin="$with_mysql_prefix/bin"
  else 
    mysql_bin=$PATH
  fi
  AC_PATH_PROG(MYSQL_CONF, mysql_config, no, $mysql_bin)
	
  have_mysql=no
  if test ! x"$MYSQL_CONF" = "xno" ; then 
    # define some variables 
    changequote(<<, >>)dnl
    MYSQL_CFLAGS=`$MYSQL_CONF --cflags | sed 's/-[ID][^ ]*//g'` 
    MYSQL_CPPFLAGS=`$MYSQL_CONF --cflags | sed 's/\(-[ID][^ ]*\)/\1/g'` 
    MYSQL_LDFLAGS=`$MYSQL_CONF --libs | sed 's/-l[^ ]*//g'`
    MYSQL_LIBS=`$MYSQL_CONF --libs | sed 's/\(-l[^ ]*\)/\1/g'`
    MYSQL_VERS=`$MYSQL_CONF --version` 
    MYSQL_PORT=`$MYSQL_CONF --port` 
    changequote([, ])dnl

    # Check the version number is OK.
    mysql_min_version=ifelse([$1], ,5.0.0,$1)
    AC_MSG_CHECKING(if MySQL client library version is >= $mysql_min_version)
    mysql_vers=`echo $MYSQL_VERS | \
      awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
    mysql_regu=`echo $mysql_min_version | \
      awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
    if test $mysql_vers -ge $mysql_regu ; then 
         have_mysql=yes
    fi
    AC_MSG_RESULT([$have_mysql - $MYSQL_VERS])
  fi
  if test "x$have_mysql" = "xyes" ; then 
    ifelse([$2], , :, [$2])
  else 
    ifelse([$3], , :, [$3])
  fi
  AC_SUBST(MYSQL_CFLAGS)
  AC_SUBST(MYSQL_LDFLAGS)
  AC_SUBST(MYSQL_CPPFLAGS)
  AC_SUBST(MYSQL_LIBS)
  AC_SUBST(MYSQL_VERS)
  AC_SUBST(MYSQL_PORT)
])		

#
# EOF
#
dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: acinclude.m4,v 1.3 2009-02-09 23:10:20 hehi Exp $ 
dnl  
dnl  ROOT generic rcuconf framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_RCUCONF([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUCONF],
[
    AC_REQUIRE([AC_RCUDB])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcuconf-prefix],
        [AC_HELP_STRING([--with-rcuconf-prefix],
		[Prefix where Rcuconf is installed])],
        rcuconf_prefix=$withval, rcuconf_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcuconf-url],
        [AC_HELP_STRING([--with-rcuconf-url],
		[Base URL where the Rcuconf dodumentation is installed])],
        rcuconf_url=$withval, rcuconf_url="")
    if test "x${RCUCONF_CONFIG+set}" != xset ; then 
        if test "x$rcuconf_prefix" != "x" ; then 
	    RCUCONF_CONFIG=$rcuconf_prefix/bin/rcuconf-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(RCUCONF_CONFIG, rcuconf-config, no)
    rcuconf_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for Rcuconf version >= $rcuconf_min_version)

    # Check if we got the script
    rcuconf_found=no    
    if test "x$RCUCONF_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       RCUCONF_CPPFLAGS=`$RCUCONF_CONFIG --cppflags`
       RCUCONF_INCLUDEDIR=`$RCUCONF_CONFIG --includedir`
       RCUCONF_LIBS=`$RCUCONF_CONFIG --libs`
       RCUCONF_LTLIBS=`$RCUCONF_CONFIG --ltlibs`
       RCUCONF_LIBDIR=`$RCUCONF_CONFIG --libdir`
       RCUCONF_LDFLAGS=`$RCUCONF_CONFIG --ldflags`
       RCUCONF_LTLDFLAGS=`$RCUCONF_CONFIG --ltldflags`
       RCUCONF_PREFIX=`$RCUCONF_CONFIG --prefix`

       # Check the version number is OK.
       rcuconf_version=`$RCUCONF_CONFIG -V` 
       rcuconf_vers=`echo $rcuconf_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       rcuconf_regu=`echo $rcuconf_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $rcuconf_vers -ge $rcuconf_regu ; then 
            rcuconf_found=yes
       fi
    fi
    AC_MSG_RESULT($rcuconf_found - is $rcuconf_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_RCUCONF, [Whether we have rcuconf])

    if test "x$rcuconf_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$RCUCONF_LIBDIR $RCUCONF_LIBS -L$RCUDB_LIBDIR $RCUDB_LIBS"
    	CPPFLAGS="$CPPFLAGS $RCUCONF_CPPFLAGS $RCUDB_UTILS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_rcuconf_channel_h=0
        AC_CHECK_HEADER([rcuconf/Configurator.h], 
	                [have_rcuconf_configurator_h=1])

        # Check the library. 
        have_librcuconf=no
        AC_MSG_CHECKING(for -lrcuconf)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <rcuconf/Sequence.h>
#include <rcudb/Server.h>],
                        [RcuDb::Server* server = RcuDb::Server::Connect("");
                         RcuConf::Sequence::Create(*server);])], 
                        [have_librcuconf=yes])
        AC_MSG_RESULT($have_librcuconf)

        if test $have_rcuconf_configurator_h -gt 0    && \
            test "x$have_librcuconf"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_RCUCONF)
        else 
            rcuconf_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
    fi

    AC_MSG_CHECKING(where the Rcuconf documentation is installed)
    if test "x$rcuconf_url" = "x" && \
	test ! "x$RCUCONF_PREFIX" = "x" ; then 
       RCUCONF_URL=${RCUCONF_PREFIX}/share/doc/rcuconf/html
    else 
	RCUCONF_URL=$rcuconf_url
    fi	
    AC_MSG_RESULT($RCUCONF_URL)
   
    if test "x$rcuconf_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUCONF_URL)
    AC_SUBST(RCUCONF_PREFIX)
    AC_SUBST(RCUCONF_CPPFLAGS)
    AC_SUBST(RCUCONF_INCLUDEDIR)
    AC_SUBST(RCUCONF_LDFLAGS)
    AC_SUBST(RCUCONF_LIBDIR)
    AC_SUBST(RCUCONF_LIBS)
    AC_SUBST(RCUCONF_LTLIBS)
    AC_SUBST(RCUCONF_LTLDFLAGS)
])

dnl
dnl EOF
dnl 
dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: acinclude.m4,v 1.3 2009-02-09 23:10:20 hehi Exp $ 
dnl  
dnl  ROOT generic intercomlayer framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl
dnl __________________________________________________________________
dnl
dnl AC_INTERCOMLAYER([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_INTERCOMLAYER],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([intercomlayer],
        [AC_HELP_STRING([--with-intercomlayer],	
                        [Prefix where InterComLayer is installed])],
	[],[with_intercomlayer="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([intercomlayer-url],
        [AC_HELP_STRING([--with-intercomlayer-url],
	  [Base URL where the InterComLayer documentation is installed])],
        intercomlayer_url=$withval, intercomlayer_url="")
    if test "x${INTERCOMLAYER_CONFIG+set}" != xset ; then 
        if test "x$with_intercomlayer" != "xno" ; then 
	    INTERCOMLAYER_CONFIG=$with_intercomlayer/bin/intercomlayer-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_intercomlayer" != "xno" ; then 
        AC_PATH_PROG(INTERCOMLAYER_CONFIG, intercomlayer-config, no)
        intercomlayer_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for InterComLayer version>=$intercomlayer_min_version)

        # Check if we got the script
        with_intercomlayer=no    
        if test "x$INTERCOMLAYER_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           INTERCOMLAYER_CPPFLAGS=`$INTERCOMLAYER_CONFIG --cppflags`
           INTERCOMLAYER_INCLUDEDIR=`$INTERCOMLAYER_CONFIG --includedir`
           INTERCOMLAYER_LIBS=`$INTERCOMLAYER_CONFIG --libs`
           INTERCOMLAYER_LTLIBS=`$INTERCOMLAYER_CONFIG --ltlibs`
           INTERCOMLAYER_LIBDIR=`$INTERCOMLAYER_CONFIG --libdir`
           INTERCOMLAYER_LDFLAGS=`$INTERCOMLAYER_CONFIG --ldflags`
           INTERCOMLAYER_LTLDFLAGS=`$INTERCOMLAYER_CONFIG --ltldflags`
           INTERCOMLAYER_PREFIX=`$INTERCOMLAYER_CONFIG --prefix`
           
           # Check the version number is OK.
           intercomlayer_version=`$INTERCOMLAYER_CONFIG -V` 
           intercomlayer_vers=`echo $intercomlayer_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           intercomlayer_regu=`echo $intercomlayer_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $intercomlayer_vers -ge $intercomlayer_regu ; then 
                with_intercomlayer=yes
           fi
        fi
        AC_MSG_RESULT($with_intercomlayer - is $intercomlayer_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_INTERCOMLAYER, [Whether we have intercomlayer])
    
    
        if test "x$with_intercomlayer" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
            LDFLAGS="$LDFLAGS $INTERCOMLAYER_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $INTERCOMLAYER_CPPFLAGS"
     
            # Change the language 
            AC_LANG_PUSH(C++)
    
     	    # Check for a header 
            have_intercomlayer_intercom_h=0
            AC_CHECK_HEADER([intercomlayer/InterCom.hpp], 
	                    [have_intercomlayer_intercom_h=1])
    
            # Check the library. 
            have_libintercomlayer=no
            AC_MSG_CHECKING(for -lInterCom)
            AC_LINK_IFELSE([
               AC_LANG_PROGRAM([#include <intercomlayer/InterCom.hpp>
#include <intercomlayer/CommandCoderBase.hpp>
#include <string>
#include <vector>
struct DummyCoco : public CommandCoderBase {
  int createDataBlock(char*,int) { return 0;}
  long int* getDataBlock() { return 0; }
  std::vector<std::string> getError() {std::vector<std::string> r; return r; }
};
CommandCoderBase* CommandCoderBase::instance = new DummyCoco;],
                               [ztt::dcs::InterCom::createInterCom()])], 
                               [have_libintercomlayer=yes])
            AC_MSG_RESULT($have_libintercomlayer)
    
            if test $have_intercomlayer_intercom_h -gt 0    && \
                test "x$have_libintercomlayer"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_INTERCOMLAYER)
            else 
                with_intercomlayer=no
            fi
            # Change the language 
            AC_LANG_POP(C++)
    	CPPFLAGS=$save_CPPFLAGS
    	LDFLAGS=$save_LDFLAGS
        fi
    
        AC_MSG_CHECKING(where the InterComLayer documentation is installed)
        if test "x$intercomlayer_url" = "x" && \
    	test ! "x$INTERCOMLAYER_PREFIX" = "x" ; then 
           INTERCOMLAYER_URL=${INTERCOMLAYER_PREFIX}/share/doc/intercomlayer
        else 
    	INTERCOMLAYER_URL=$intercomlayer_url
        fi	
        AC_MSG_RESULT($INTERCOMLAYER_URL)
    fi
   
    if test "x$with_intercomlayer" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(INTERCOMLAYER_URL)
    AC_SUBST(INTERCOMLAYER_PREFIX)
    AC_SUBST(INTERCOMLAYER_CPPFLAGS)
    AC_SUBST(INTERCOMLAYER_INCLUDEDIR)
    AC_SUBST(INTERCOMLAYER_LDFLAGS)
    AC_SUBST(INTERCOMLAYER_LIBDIR)
    AC_SUBST(INTERCOMLAYER_LIBS)
    AC_SUBST(INTERCOMLAYER_LTLIBS)
    AC_SUBST(INTERCOMLAYER_LTLDFLAGS)
])
dnl
dnl EOF
dnl 
dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: acinclude.m4,v 1.3 2009-02-09 23:10:20 hehi Exp $ 
dnl  
dnl  ROOT generic rcudb framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_RCUDB([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUDB],
[
    # AC_REQUIRE([AC_RCUXX])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcudb-prefix],
        [AC_HELP_STRING([--with-rcudb-prefix],
		[Prefix where RcuDb is installed])],
        rcudb_prefix=$withval, rcudb_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcudb-url],
        [AC_HELP_STRING([--with-rcudb-url],
		[Base URL where the RcuDb dodumentation is installed])],
        rcudb_url=$withval, rcudb_url="")
    if test "x${RCUDB_CONFIG+set}" != xset ; then 
        if test "x$rcudb_prefix" != "x" ; then 
	    RCUDB_CONFIG=$rcudb_prefix/bin/rcudb-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(RCUDB_CONFIG, rcudb-config, no)
    rcudb_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for RcuDb version >= $rcudb_min_version)

    # Check if we got the script
    rcudb_found=no    
    if test "x$RCUDB_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       RCUDB_CPPFLAGS=`$RCUDB_CONFIG --cppflags`
       RCUDB_INCLUDEDIR=`$RCUDB_CONFIG --includedir`
       RCUDB_LIBS=`$RCUDB_CONFIG --libs`
       RCUDB_LTLIBS=`$RCUDB_CONFIG --ltlibs`
       RCUDB_LIBDIR=`$RCUDB_CONFIG --libdir`
       RCUDB_LDFLAGS=`$RCUDB_CONFIG --ldflags`
       RCUDB_LTLDFLAGS=`$RCUDB_CONFIG --ltldflags`
       RCUDB_PREFIX=`$RCUDB_CONFIG --prefix`

       # Check the version number is OK.
       rcudb_version=`$RCUDB_CONFIG -V` 
       rcudb_vers=`echo $rcudb_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       rcudb_regu=`echo $rcudb_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $rcudb_vers -ge $rcudb_regu ; then 
            rcudb_found=yes
       fi
    fi
    AC_MSG_RESULT($rcudb_found - is $rcudb_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_RCUDB, [Whether we have rcudb])

    if test "x$rcudb_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$RCUDB_LIBDIR $RCUDB_LIBS"
    	CPPFLAGS="$CPPFLAGS $RCUDB_CPPFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_rcudb_server_h=0
        AC_CHECK_HEADER([rcudb/Server.h], 
	                [have_rcudb_server_h=1])

        # Check the library. 
        have_librcudb=no
        AC_MSG_CHECKING(for -lrcudb)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <rcudb/Sql.h>],
                        [new RcuDb::Sql("");])], 
                        [have_librcudb=yes])
        AC_MSG_RESULT($have_librcudb)

        if test $have_rcudb_server_h -gt 0    && \
            test "x$have_librcudb"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_RCUDB)
        else 
            rcudb_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
    fi

    AC_MSG_CHECKING(where the RcuDb documentation is installed)
    if test "x$rcudb_url" = "x" && \
	test ! "x$RCUDB_PREFIX" = "x" ; then 
       RCUDB_URL=${RCUDB_PREFIX}/share/doc/rcudb/html
    else 
	RCUDB_URL=$rcudb_url
    fi	
    AC_MSG_RESULT($RCUDB_URL)
   
    if test "x$rcudb_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUDB_URL)
    AC_SUBST(RCUDB_PREFIX)
    AC_SUBST(RCUDB_CPPFLAGS)
    AC_SUBST(RCUDB_INCLUDEDIR)
    AC_SUBST(RCUDB_LDFLAGS)
    AC_SUBST(RCUDB_LIBDIR)
    AC_SUBST(RCUDB_LIBS)
    AC_SUBST(RCUDB_LTLIBS)
    AC_SUBST(RCUDB_LTLDFLAGS)
])

dnl
dnl EOF
dnl 
