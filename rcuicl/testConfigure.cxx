#include "config.h"
#include <string>
#include "Options.h"
#include <cstring>
#include <dim/dic.hxx>

// extern "C" {
//   extern int dim_set_dns_node(char* node);
//   extern int dim_set_dns_port(int port);
//   extern int dic_cmnd_service_(char* name, void* data, int size);
// }

struct  config_struct
{
  char target[20];
  int  tag[1];
};

int 
main(int argc, char** argv) 
{
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<std::string> tOpt('t', "target",  "FeeServer target", "");
  Option<int>         TOpt('T', "tag",     "Configuration tag", 1);
  Option<std::string> dOpt('d', "dns",     "DIM DNS node", "localhost");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(tOpt);
  cl.Add(TOpt);
  cl.Add(dOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << PACKAGE_STRING << std::endl;
    return 0;
  }

  DimClient::setDnsNode(const_cast<char*>(dOpt->c_str()));  
  // dim_set_dns_node(const_cast<char*>(dOpt->c_str()));
  setenv("DIM_DNS_NODE", dOpt->c_str(), 0);

  if (tOpt->empty()) {
    std::cerr << "No target specified" << std::endl;
    return 1;
  }
  config_struct cs;
  size_t n = std::max(tOpt->size(), 19U);
  strncpy(cs.target, tOpt->c_str(), n);
  for (size_t i = n; i < 20; i++)  cs.target[n] = '\0';
  // for (size_t i = 0; i < 256; i++) cs.tag[i]    = -1;
  cs.tag[0] = TOpt;

  std::cout << "Sending command: ConfigureFero" << std::endl;
  // int ret = dic_cmnd_service_("ConfigureFero", &cs, sizeof(config_struct));
  int ret = DimClient::sendCommand("ConfigureFero", &cs, 
				   sizeof(config_struct));
  
  return ret;
}
