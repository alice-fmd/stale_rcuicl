// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuconf.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Feb 22 00:01:27 2007
    @brief   Program that configures the front-end from DB look-ups,
    and using the Rcu++ abstraction layer.
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#else 
# define PACKAGE "rcuicl"
# define VERSION "?.?"
# define PACKAGE_STRING "Rcu InterCom Layer version ?.?"
#endif
#include <rcuconf/CommandCoder.h>
#include <intercomlayer/InterCom.hpp>
#include <intercomlayer/fee_loglevels.h>
#include <intercomlayer/FedMessenger.hpp>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include "Options.h"

extern "C" {
  extern int dim_set_dns_node(char* node);
  extern int dim_set_dns_port(int port);
}

struct sys_exception : public std::exception 
{
  sys_exception(int err) : _what(strerror(err)) {}
  virtual ~sys_exception() throw() {}
  const char* what() const throw() { return _what.c_str(); }
  std::string _what;
};


/** PID file */
std::string pid_file;
bool        done = false;

/** Signal handler */ 
void signalHandler(int sig) 
{
  if (!pid_file.empty()) remove(pid_file.c_str());
  pid_file = "";
  done = true;
  // exit(0);
}


/** Main function 
    @param argc Number of command line arguments
    @param argv Vector of command line arguments
    @return 0 on success, error code otherwise */
int
main(int argc, char** argv) 
{
  using namespace RcuConf;
  Option<bool>        hOpt('h', "help",    "Show this help", false,false);
  Option<bool>        VOpt('V', "version", "Show version number", false,false);
  Option<bool>        iOpt('i', "interactive", "Run interactively", false,false);
  Option<std::string> cOpt('c', "config",  "Configuration directory", 
			   CONFIG_DIR);
  Option<std::string> pOpt('p', "pid",     "PID file", PID_FILE);
  Option<std::string> dOpt('d', "dns",     "DIM DNS node", "localhost");
  CommandLine cl("");
  cl.Add(hOpt);
  cl.Add(VOpt);
  cl.Add(cOpt);
  cl.Add(pOpt);
  cl.Add(iOpt);
  cl.Add(dOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt.IsSet()) {
    cl.Help();
    return 0;
  }
  if (VOpt.IsSet()) {
    std::cout << PACKAGE_STRING << std::endl;
    return 0;
  }

  
  std::string         conf_dir = cOpt;
  std::string         dns      = dOpt;
  pid_file                     = pOpt;
  int                 ret      = 0;
  bool                inter    = iOpt;
  ztt::dcs::InterCom* interCom = 0;
  try {
    int parent = (inter ? 0 : fork());
    if (parent < 0) 
      throw sys_exception(errno);
    if (parent) {
      // We're in the parent - exit immediately 
      ret = 0;
    }
    else {
      // We're in the child.  
      std::cout << "We're the parent\n" 
		<< "PWD=" << getenv("PWD") << std::endl;
      dim_set_dns_node(const_cast<char*>(dns.c_str()));
      setenv("DIM_DNS_NODE", dns.c_str(), 0);
      
      // If we're interactive, don't install signal handlers etc. 
      if (!inter) {
	// set the signal handler
	signal(SIGTERM,signalHandler);
	signal(SIGINT, signalHandler);
#ifndef _WIN32
	signal(SIGQUIT,signalHandler);
#else
	signal(SIGABRT,signalHandler);
#endif
#ifdef SIGTTOU
	signal(SIGTTOU, SIG_IGN);
#endif
#ifdef SIGTTIN
	signal(SIGTTIN, SIG_IGN);
#endif
#ifdef SIGTSTP
	signal(SIGTSTP, SIG_IGN);
#endif
	// signal(SIGHUP,signal_handler);

	// Make this a process leader 
	if (setsid() == -1) throw sys_exception(errno);

	// Get the PID of this process, and write to the pid file 
	pid_t pid = getpid();
	std::ofstream pidf(pid_file.c_str());
	if (!pidf) {
	  std::stringstream s;
	  s << "Failed to create pid file: " << pid_file;
	  throw std::runtime_error(s.str());
	}
	pidf << pid << std::endl;
	pidf.close();
      }
      
      // Make sure there's a slash at the end. 
      if (conf_dir[conf_dir.size()-1] != '/') conf_dir += "/";

      // set the configuration directory for the core ICL 
      ztt::dcs::InterCom::setConfigDir(conf_dir);

      // Append configuration file to configuration directory 
      std::string conf_file = conf_dir + "config";
      
      // set the configuration file 
      RcuConf::setConfigFile(conf_file);
    
      // And then start the process. 
      ztt::dcs::InterCom* interCom = ztt::dcs::InterCom::createInterCom();
      interCom->setUp();
      interCom->run(inter);

      // Loop forever, or until we get a message saying `stop'
      if (!inter) while (!done) pause();
      
      // Remove pid file after termination 
      remove(pid_file.c_str());
    }
  }
  catch(std::logic_error& e){
    if (interCom) interCom->setLogEntry(Msg_Alarm, SOURCE_LOCAL, 
					(char*) e.what());
    std::cerr << e.what() << std::endl;
    ret = 1;
  }
  catch (std::exception& ex) {
    if (interCom) interCom->setLogEntry(Msg_Alarm, SOURCE_LOCAL, 
					(char*) ex.what());
    std::cerr << ex.what() << std::endl;
    ret = 2;
  }
  catch (...) {
    if (interCom) interCom->setLogEntry(Msg_Alarm, SOURCE_LOCAL,
					"Unknown exception occured!");
    std::cerr << "Unknown exception" << std::endl;
    ret = 3;
  }
  return ret;
}

//
// EOF
//

    
      
    
    


